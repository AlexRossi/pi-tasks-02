//�������� ���������, ������������ ��������� ������������� �������� 
//����� � ������������������ �����, ��������: 
//"3-12" ->[3, 4, 5, 6, 7, 8, 9, 10, 11, 12].
//����� ����������� ������ ����� � �������������.

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int x1=0;
	int x2=0;
	int i=0;
	puts("Enter interval format * - *\n");
	scanf("%d - %d", &x1, &x2);
	if (x1 > x2)
	{
		puts("System ERROR!");
			return 1;
	}
	else
	{
		printf("'%d - %d' -> [", x1, x2);
		for (i = x1; i <= x2; i++)
		{
			if (i < x2)
				printf("%d,", i);
			else
				printf("%d", i);
			
		}
		putchar(']');
	}
	return 0;
}