#include <stdio.h>
#include <string.h>

int main()
{
	const char key[256] = "Slipknot";		//строковая константа
	char password[256];
	int i = 0, k = 0;
	printf("Enter a password: ");
	while (password[i - 1] != 13)
		{
		    password[i] = getch();
		    if (password[i] == key[i])
			    k++;
		    if (password[i] != 13)
			    putchar('*');
		    else
			    password[i + 1] = '\0';
		    i++;
		}
	if (k == strlen(key) && k == i - 1)
		printf("\nPassword is correct.\n");
	else
		printf("\nPassword is not correct!\n");
	return 0;
}